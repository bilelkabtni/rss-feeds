import './App.scss';

import React from 'react';
import RssFeedForm from './components/RssFeedForm';

const App = () => {
  return (
    <div className="container">
      <RssFeedForm />
    </div>
  );
}

export default App;
