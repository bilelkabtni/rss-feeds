import React, {useEffect, useState} from "react";

import Error from '../components/Error';
import ErrorMessages from '../enum/errorMsg';
import FeedCard from '../components/Card';
import LoadingSpinner from '../components/LoadingSpinner';
import Pagination from "react-js-pagination";
import {UseParser} from "../helpers/UseParser";
import ValidateUrl from "../helpers/ValidateUrl";
import {useForm} from "../helpers/useForm";

const RssFeedForm = () => {
    const [values, handleChange] = useForm({rssFeedUrl: ''});
    const [data, setData] = useState({feeds: null, loading: false});
    const [disabled, setDisabled] = useState(true);
    const [validUrl, setValidUrl] = useState(false);
    const [error, setError] = useState(false);
    const [message, setMessage] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);


    const [totalRecords, setTotalRecords] = useState(1);
    const [pageContent, setPageContent] = useState([]);

    // total records per page to display
    const recordPerPage = 5;

    // range of pages in paginator  
    const pageRange = 5;

    const FetchFeed = async (e) => {
        e.preventDefault();
        setData({feeds: null, loading: true});

        UseParser(values.rssFeedUrl).then(feeds => {
            setData(feeds);
            setTotalRecords(feeds.feeds.items.length);
            setPageContent(feeds.feeds.items.slice(0, recordPerPage));
        }, err => {
            setData({feeds: null, loading: false});
            setError(true);
            setMessage(ErrorMessages.errorRequest)
        })
    }

    // handle change event.
    const handlePageChange = pageNumber => {
        setCurrentPage(pageNumber);
        let feeds;
        feeds = data
            .feeds
            .items
            .slice(recordPerPage * pageNumber, (pageNumber + 1) * recordPerPage)
        setPageContent(feeds);
    }

    useEffect(() => {
        setValidUrl(ValidateUrl(values.rssFeedUrl));
        if(!validUrl) {
            setPageContent([]);
            setData([]);
        }
        values.rssFeedUrl
            ?.length > 0
                ? setDisabled(false)
                : setDisabled(true);
        setError(false);
    }, [values.rssFeedUrl])

    return (
        <form className="rssFeedForm" name="rssFeedForm" onSubmit={FetchFeed}>

            <label htmlFor="basic-url">Please Enter a valid RSS Url:</label>
            <div className="input-group mb-3">
                <div className="input-group-prepend">
                    <span className="input-group-text" id="basic-addon3">Example: https://www.reddit.com/.rss</span>
                </div>
                <input
                    type="text"
                    className="form-control"
                    name="rssFeedUrl"
                    value={values.rssFeedUrl}
                    onChange={handleChange}
                    id="basic-url"
                    aria-describedby="basic-addon3"/>
            </div>
            <Error error={error} message={message}/>
            {!validUrl && values.rssFeedUrl && <Error error={true} message={ErrorMessages.invalidUrl}/>}
            <div className="flex">
                <button
                    type="submit"
                    className={error
                        ? "btn btn-danger"
                        : "btn btn-success"}
                    disabled={disabled || !validUrl}>Fetch Feed</button>
                <LoadingSpinner loading={data.loading}/>
            </div>

            <div className="grid-container">
                {
                   validUrl && data.feeds && pageContent.map((feed => {
                        return (<FeedCard feed={feed} key={feed.id}/>)
                    }))
                }
            </div>

            <div className="flex-center">
                { validUrl && data.feeds &&
                <Pagination
                    itemClass="page-item"
                    linkClass="page-link"
                    activePage={currentPage}
                    itemsCountPerPage={recordPerPage}
                    totalItemsCount={totalRecords}
                    pageRangeDisplayed={pageRange}
                    onChange={handlePageChange}/>
                }
            </div>
        </form>
    )
}

export default RssFeedForm;