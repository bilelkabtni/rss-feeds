import React from "react";

const LoadingSpinner = ({
    ...props
}) => {
    const {loading} = props;
    return (
        <div className="flex-center spinner-cmp">
            {loading &&< div className = "spinner-border" role = "status" > <span className="sr-only">Loading...</span> </div>}
        </div>
    );
};

export default LoadingSpinner;