import React from "react";

const Error = ({
    ...props
}) => {
    const {error, message} = props;
    return (
        <div>
            {
                error && <div className="alert alert-danger" role="alert">
                        <p className='error'>{message}</p>
                    </div>
            }
        </div>
    );
};

export default Error;