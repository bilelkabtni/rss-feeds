import React from "react";
import moment from "moment";

const FeedCard = ({
    ...props
}) => {
    const {feed} = props;
    return (
        <div>
            <div className="flex">
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title text-for-lines" title={feed.title}>{feed.title}</h5>
                        <p className="card-text"><strong>Author:</strong> {feed.author}</p>
                        <p className="card-text"><strong>Date:</strong> {moment(feed.pubDate ).format("YYYY/MM/DD")}</p>
                        <a href={feed.link} title={feed.link} className="btn btn-light" target="_blank">Go to feed</a>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FeedCard;