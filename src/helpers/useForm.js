import { useState } from "react";

// helper methode to set form values
export const useForm = initialValues => {
  const [values, setValues] = useState(initialValues);
  return [
    values,
    e => {
      setValues({
        ...values,
        [e.target.name]: e.target.value
      });
    }
  ];
};