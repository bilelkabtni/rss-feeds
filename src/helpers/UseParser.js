import * as Parser from "rss-parser";

const CORS_PROXY = "https://cors-anywhere.herokuapp.com/";
let loading = true;

const getFeeds = (url) => {
    return new Promise((resolve, reject) => {
        loading = true;
        const parser = new Parser();
        parser.parseURL(CORS_PROXY + url, (err, feeds) => {
            if (err) 
                reject(new Error(err));
            loading = false
            resolve({feeds, loading})
        });
    });
}

// fetch api
export const UseParser = async url => {
    return await getFeeds(url);
};