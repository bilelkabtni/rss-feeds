describe('Rss feed', () => {
    it('should fetch example url', () => {
        cy.visit('http://localhost:3000/');
        cy.get('.rssFeedForm').should('be.visible');
        cy.get(':button').should('be.disabled');
        cy.get('#basic-url').should('be.visible').type('https://www.reddit.com/.rss');
        cy.get(':button').should('not.be.disabled').click();
        cy.get('.spinner-cmp').should('be.visible');
        cy.screenshot();
        cy.wait(1000);
        cy.get('.grid-container').should('be.visible');  
        cy.get('.page-item').eq(3).should('be.visible').click();  
        cy.get('.grid-container').should('be.visible');  
        cy.screenshot();
    })
    it('should return an error', () => {
        cy.visit('http://localhost:3000/');
        cy.get('.rssFeedForm').should('be.visible');
        cy.get('#basic-url').should('be.visible').type('https://www.reddit.com/.rsss');
        cy.get(':button').should('not.be.disabled').click();
        cy.screenshot();
        cy.wait(1000);
        cy.get('.alert-danger').find('p.error').contains('something wrong! please try again.').should('be.visible');
        cy.get('button.btn-danger').should('be.visible');
        cy.screenshot();
    })
})