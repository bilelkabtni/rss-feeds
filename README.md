# Rss Feeds
Simple app to fetch Rss feeds

## Description
This project is build by using React and React-cli

## Installation Steps

Yarn install or npm install

## Run the project

Yarn start or npm start

## Run test

yarn run cypress or npm run cypress

## Demo
![Rss Feeds demo](rssFeeds-demo.webm)
